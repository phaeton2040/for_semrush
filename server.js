var express = require('express');
var app = express();
var path = require('path');
var mainData = require('./store/main');
var graphData = require('./store/graph');
var tableData = require('./store/table');

app.use(express.static(path.join(__dirname, 'public')));

// отдаем главную и единственную страницу
app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.get('/api/main', function(req, res) {
    res.json(mainData);
});

app.get('/api/metrics', function(req, res) {
    res.json(graphData);
});

app.get('/api/table', function(req, res) {
    res.json(tableData);
});

app.listen(3000, function(){
    console.log('Server listening on port 3000');
});