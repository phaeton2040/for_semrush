var webpack = require("webpack");

var PROD = JSON.parse(process.env.PROD_ENV || '0');

module.exports = {
	entry: [
		'./src/index.js'
	],
	output: {
		path: __dirname,
		filename: 'public/js/main.js'
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react', 'stage-0'],
					plugins: ['transform-decorators-legacy']
				}
			}
		]
	},
    devtool: PROD ? 'cheap-eval-source-map' : null,
    plugins: PROD ? [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        })
    ] : []
}