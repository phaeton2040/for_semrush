export default {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Some title'
        },
        xAxis: {
        		title: {
            		text: 'Month'
            },
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul']
        },
        yAxis: {
        		title: {
            		text: 'Traffic'
            },
            labels: {
                formatter: function () {
                    return this.value + 'M';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 2,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        }
    }