import React, {Component} from 'react';
import {Table, Column, Cell} from 'fixed-data-table';
import $ from 'jquery';

const DateCell = ({rowIndex, data, col, ...props}) => {
    const date = new Date(data[rowIndex][col] * 1000);
    const month = date.getMonth().toString().length === 1 ? `0${date.getMonth()}` : date.getMonth();
    const day = date.getDate().toString().length === 1 ? `0${date.getDate()}` : date.getDate();
    const year = date.getFullYear();
    const formattedDate = `${day}.${month}.${year}`;
    
    return (
      <Cell {...props}>
        {formattedDate}
      </Cell>)
};

class DataTable extends Component{
	constructor(props){
		super(props);
		this.state = {
            data: []
        }
	}
    
    componentWillReceiveProps(nextProps) {
        if (!this.state.init) {
            if (this.props.tabName === nextProps.tab) {
                $.ajax({
                    url: '/api/table',
                    type: 'get',
                    dataType: 'json',
                    success: response => {
                        this.setState(() => ({
                            data: response,
                            init: this.props.tabName === nextProps.tab
                        }))
                    }
                })
            }
        }
    }
    
	render(){
        const {data} = this.state;

		return(
			<div className="table-tab">
			    <Table
                    rowHeight={50}
                    headerHeight={50}
                    rowsCount={data.length}
                    width={1140}
                    height={300}>
                    <Column
                      header={<Cell>Title & URL</Cell>}
                      cell={({rowIndex, ...props}) => (
                        <Cell {...props}>
                          <div>{data[rowIndex]['source_title']}</div>
                          <div>{data[rowIndex]['source_url']}</div>
                        </Cell>
                      )}
                      fixed={true}
                      width={740}
                    />
                    <Column
                      header={<Cell>Domains</Cell>}
                      cell={({rowIndex, ...props}) => (
                        <Cell {...props}>
                          {data[rowIndex]['domains_num']}
                        </Cell>
                      )}
                      fixed={true}
                      width={100}
                    />
                    <Column
                      header={<Cell>External Links</Cell>}
                      cell={({rowIndex, ...props}) => (
                        <Cell {...props}>
                          {data[rowIndex]['external_link_num']}
                        </Cell>
                      )}
                      fixed={true}
                      width={100}
                    />
                    <Column
                      header={<Cell>Internal Links</Cell>}
                      cell={({rowIndex, ...props}) => (
                        <Cell {...props}>
                          {data[rowIndex]['internal_link_num']}
                        </Cell>
                      )}
                      fixed={true}
                      width={100}
                    />
                    <Column
                      header={<Cell>Last Seen</Cell>}
                      cell={<DateCell data={data} col="last_seen" />}
                      fixed={true}
                      width={100}
                    />
              </Table>
			</div>
		)
	}
}

export default DataTable;