import React, {Component} from 'react';
import $ from 'jquery';
import GraphConfig from './GraphConfig.js';
import ReactHighcharts from 'react-highcharts';

class Graph extends Component{
	constructor(props){
		super(props);
		this.state = {
            data: [],
            init: false
        }
	}
    
    componentWillReceiveProps(nextProps) {
//        console.dir(nextProps);
        if (!this.state.init) {
            if (this.props.tabName === nextProps.tab) {
                $.ajax({
                    url: '/api/metrics',
                    type: 'get',
                    dataType: 'json',
                    success: response => {
                        this.setState(() => ({
                            data: response,
                            init: this.props.tabName === nextProps.tab
                        }))
                    }
                })
            }
        }
    }
    
	render(){
        const {data} = this.state;
        
        if (data.length === 0) {
            return null;
        }
        
        const series = [{
            name: data[0].name,
            marker: {
                symbol: 'circle'
            },
            color: 'orange',
            data: data[0].data
        }, {
            name: data[1].name,
            marker: {
                symbol: 'circle'
            },
            color: 'blue',
            data: data[1].data
        }];
        
        GraphConfig.series = series;

		return(
			<div className="graph-tab">
			    <ReactHighcharts config = {GraphConfig}></ReactHighcharts>
			</div>
		)
	}
}

export default Graph;