import React, {Component} from 'react';
import $ from 'jquery';

class App extends Component{
	constructor(props){
		super(props);
		this.state = {
            data: {}
        }
	}

componentWillReceiveProps(nextProps) {
        if (!this.state.init) {
            if (this.props.tabName === nextProps.tab) {
                $.ajax({
                    url: '/api/main',
                    type: 'get',
                    dataType: 'json',
                    success: response => {
                        this.setState(() => ({
                            data: response,
                            init: this.props.tabName === nextProps.tab
                        }))
                    }
                })
            }
        }
    }
    
	render(){

		return(
			<div className="main-tab">
			    <h1>{this.state.data.title}</h1>
			    <p>{this.state.data.text}</p>
			</div>
		)
	}
}

export default App;