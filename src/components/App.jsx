import React, {Component} from 'react';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import Main from './Main.jsx';
import Graph from './Graph.jsx';
import DataTable from './DataTable.jsx';

class App extends Component{
	constructor(props){
		super(props);
		this.state = {
            tab: 'main'
        }
	}
    
    handleSelect = tab => {
        this.setState(() => ({
            tab: tab,
        }));
    }

	render(){
		return(
			<div className="container" style={{marginTop: 20}}>
			    <Tabs activeKey={this.state.tab} onSelect={this.handleSelect} id="main-tab-nav">
                    <Tab eventKey="main" title="Главная"><Main tab={this.state.tab} tabName="main"/></Tab>
                    <Tab eventKey="graph" title="График"><Graph tab={this.state.tab} tabName="graph" /></Tab>
                    <Tab eventKey="table" title="Таблица"><DataTable tab={this.state.tab} tabName="table" /></Tab>
                    <Tab eventKey="report" title="Отчет">
                        <Graph tab={this.state.tab} tabName="report" />
                        <DataTable tab={this.state.tab} tabName="report" />
                    </Tab>
                </Tabs>
			</div>
		)
	}
}

export default App